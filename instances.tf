resource "google_compute_instance" "vm_instance_2" {
  name         = "terraform-instance-2"
  machine_type = "e2-medium"
 

  boot_disk {
    initialize_params {
      image = "centos-7-v20200403"
    }
  }
  tags = [ "rdp-allow1", ]

  network_interface {
   network = "default"

   access_config {
     // Include this section to give the VM an external ip address
   }
 }

}