resource "google_compute_firewall" "rdp-allow" {
    name = "allow-http"
    network = "default"
    direction = "INGRESS"
    source_ranges = ["0.0.0.0/0"]
    target_tags = [ "rdp-allow" ]
    allow {
      protocol = "tcp"
      ports = [ "3389" ]
    }
}